# Takes conf rules from ./conf, can be overrided by pass conf variable:
# make conf=custom.conf
# writted for GNU Make, so:
# IMPORTANT: should be run by gmake under FreeBSD

conf = conf

# allow comments that start with #
define sed-rules
	$(shell cat $(conf) | egrep -v "^\s*#")
endef

all: conf.py init run.wsgi

conf.py: conf.py.template conf Makefile
	@echo Создаем conf.py
	@sed $(foreach rule, $(sed-rules), -e $(rule)) < conf.py.template > conf.py

init: init.template conf Makefile
	@echo Создаем init
	@sed $(foreach rule, $(sed-rules), -e $(rule)) < init.template > init
	@echo Даем права на запуск init
	@chmod u+x init

httpd.conf: httpd.conf.template conf Makefile
	@echo Создаем httpd.conf
	@sed $(foreach rule, $(sed-rules), -e $(rule)) < httpd.conf.template > httpd.conf

run.wsgi: run.wsgi.template conf Makefile
	@echo Создаем run.wsgi
	@sed $(foreach rule, $(sed-rules), -e $(rule)) < run.wsgi.template > run.wsgi

.PHONY : clean
clean :
	rm -f conf.py init

