# -*- encoding: utf-8 -*-

from datetime import date, datetime, timedelta
from django.db import models
from django.contrib.auth.models import User
from django.core import serializers
from django.conf import settings

SESSION_LENGTH = 1800 # seconds = 30 min
SESSION_BETWEEN = 1200 # seconds = 20 min
SESSION_READERS_LIMIT = 5
STATUS_OPEN = 1
STATUS_CLOSED = 2
STATUS_TALK = 3
STATUS_FINISHED = 4
statuses = {
    STATUS_OPEN: u'Идёт запись',    
    STATUS_CLOSED: u'Закрыт для записи',    
    STATUS_TALK: u'Идёт разговор',    
    STATUS_FINISHED: u'Завершён',    
        }

def serialize(obj):
    if obj is None: return obj
    JSONSerializer = serializers.get_serializer("json")
    json_serializer = JSONSerializer()
    json_serializer.serialize([obj])
    return json_serializer.getvalue()

class Book(models.Model):
    title = models.CharField(u'Название', max_length=200)
    name = models.CharField(u'Имя', max_length=200)
    slug = models.SlugField()

    class Meta:
        verbose_name = u'Книга'
        verbose_name_plural = u'Книги'

    def __unicode__(self):
        return "%s" % (self.title, )

    def is_available(self):
        return self.session_set.filter(status__id__in=[STATUS_OPEN, STATUS_CLOSED]).count() == 0

    def will_be_available_from(self, now):
        #TODO check STATUS_CLOSED also
        session_in_talk = Session.objects.filter(status__id=STATUS_TALK, book=self)
        if session_in_talk.count():
            # FIXED start of the next session
            start = session_in_talk[0].finish + timedelta(seconds=SESSION_BETWEEN)
        else:
            start = now + timedelta(seconds=SESSION_BETWEEN)
        return start


class Reader(models.Model):
    number = models.IntegerField(u'Номер', unique=True)
    name = models.CharField(u'Имя', max_length=200, blank=True, default='')

    class Meta:
        verbose_name = u'Читатель'
        verbose_name_plural = u'Читатели'

    def __unicode__(self):
        return "%s.%s" % (self.name, self.number)

    def can_read_more(self):
        csessions = self.session_set.exclude(status__id=STATUS_FINISHED)
        return csessions.count() > 0, csessions[0] if csessions.count() else None 
    def total_sessions(self):
        return self.session_set.count()

    def sessions_as_str(self):
        return u", ".join([u"%s в %s" % (s.book.title, s.start.strftime("%H:%M")) for s in self.session_set.all()])


class Status(models.Model):
    name = models.CharField(u'Описание', max_length=200)
    slug = models.SlugField()

    class Meta:
        verbose_name = u'Статус сеанса чтения'
        verbose_name_plural = u'Статусы сеансов чтения'

    def __unicode__(self):
        return self.name



class Session(models.Model):
    start = models.DateTimeField(u'Начало разговора')#, auto_now_add=True, editable=True)
    finish = models.DateTimeField(u'Окончание разговора')#, auto_now_add=True, editable=True)
    last_modified = models.DateTimeField(u'Время последнего обновления', 
            auto_now = True)
    book = models.ForeignKey(Book, verbose_name=u'Книга')
    readers = models.ManyToManyField(
        Reader, verbose_name = u'Читатели', blank = True, null = True)
    status = models.ForeignKey(Status, verbose_name=u'Статус сеанса чтения', 
            default=STATUS_OPEN)
    class Meta:
        verbose_name = u'Сеанс чтения'
        verbose_name_plural = u'Сеансы чтения'
        ordering = ['start']

    def __unicode__(self):
        return '%s %s' % (self.start.strftime("%H:%M"), self.book)

    def reader_ids(self):
        return str([r.id for r in self.readers.all()])[1:-1]

    def readers_as_str(self):
        return u", ".join([str(r) for r in self.readers.all()])

    def is_editable(self):
        return self.status_id in [STATUS_OPEN, STATUS_CLOSED]

    def is_reader_editable(self):
        # правим читателей почти всегда
        return self.status_id != STATUS_FINISHED

    def anti_status_id(self):
        """ Открытый статус делает закрытым, остальные -- открытым """
        return STATUS_OPEN if (self.status_id - 1) else STATUS_CLOSED

    def anti_status(self):
        return statuses.get(self.anti_status_id(), None)
    
    @property
    def ends(self):
        #return self.start + timedelta(seconds=SESSION_LENGTH)
        return self.finish
    
    @property
    def since_end(self):
        now = datetime.now()
        return now - self.finish if self.finish > now else None

    def begins_after(self):
        now = datetime.now()
        return (self.start - now).seconds/60 \
                if self.start > now else None

    def available_after(self):
        stop = self.finish #self.start + timedelta(seconds=SESSION_LENGTH)
        now = datetime.now()
        return (stop - now).seconds/60 \
                if self.start < now and now < stop else None

    def start_as_time(self):
        return self.start.strftime(settings.TIME_FORMAT)

    def finish_as_time(self):
        return self.finish.strftime(settings.TIME_FORMAT)
