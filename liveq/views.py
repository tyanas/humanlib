# -*- coding: utf-8 -*-

import json
from datetime import datetime, timedelta

from annoying.decorators import render_to
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.template import RequestContext

from liveq.models import Book, Reader, Session, Status, serialize, \
        STATUS_OPEN, STATUS_CLOSED, STATUS_TALK, STATUS_FINISHED, \
        SESSION_READERS_LIMIT, SESSION_LENGTH, SESSION_BETWEEN
        
from liveq.forms import SessionForm, ReaderForm

TIMEOUT_INTERVALS = {
    'edit':30000, # 30 sec
    'open':30000, # 30 sec
    'talk':60000, # 60 sec
    'finished':300000, # 300 sec = 5 min
        }

def json_response(obj):
    return HttpResponse(json.dumps(obj), mimetype='text/plain')

def html_response(txt):
    return HttpResponse(txt)

def good(msg=u"Ок"):
    return u"<span class='info'>%s</span>" % msg

def bad(msg=u"Ошибка"):
    return u"<span class='errors'>%s</span>" % msg

def check_statuses(now):
    talk_start = now - timedelta(seconds=SESSION_LENGTH)
    
    actual_sessions = Session.objects.exclude(status__id=STATUS_FINISHED)

    # выбираем сессии, которым уже пора начаться
    tsessions = actual_sessions.filter(
            status__id__in=[STATUS_OPEN, STATUS_CLOSED], start__lt=now)
    status_talk = Status.objects.get(pk=STATUS_TALK)
    for s in tsessions:
        # меняем статус на "идет разговор", если есть читатели
        if s.readers.count():
            s.status = status_talk
        else:
            s.start = s.book.will_be_available_from(now)
            s.finish = s.start + timedelta(seconds=SESSION_LENGTH)
        s.save()

    # выбираем сессии, которым уже пора закончится
    # FIXED определение завершенных сессий
    fsessions = actual_sessions.filter(
            status__id=STATUS_TALK, finish__lt=now)
    status_finished = Status.objects.get(pk=STATUS_FINISHED)
    for s in fsessions:
        # меняем статус на "разговор завершен"
        s.status = status_finished
        s.save()

    # книги без сеансов
    new_sessions = 0
    books = Book.objects.all()
    for book in books:
        if book.is_available():
            start_new_session(book, now)
            new_sessions += 1

    return u'Добавлено новых сессий: %s'%new_sessions

@login_required
def x_check_statuses(request):
    res = check_statuses(datetime.now())
    return html_response(good(res))



def start_new_session(book, now):
    st = book.will_be_available_from(now)
    talk_len = timedelta(seconds=SESSION_LENGTH)
    s = Session(book=book, start=st, finish=st+talk_len,
            status_id=STATUS_OPEN)
    s.save()
    return s


def actual_sessions():
    return Session.objects.exclude(status__id=STATUS_FINISHED).order_by('start')


@login_required
@render_to('liveq/add.html')
def add(request):
    now = datetime.now()
    form = SessionForm()
    reader_form = ReaderForm()
    return {
            'form': form, 
            'reader_form': reader_form, 
            'sessions': actual_sessions(),
            'timeout_interval': TIMEOUT_INTERVALS['edit'],
            'refresh_url': reverse('x_edit_sessions'),
            'now': now,
            'content_title':u'Запись читателей',
            'mode': 'edit',
            }



@render_to('liveq/_table_form.html')
def x_edit_sessions(request):
    sessions = Session.objects.filter(status__id__in=[STATUS_OPEN, 
        STATUS_CLOSED, STATUS_TALK]).order_by('status')
    check_statuses(datetime.now())
    return {'sessions': sessions,
            'mode': 'edit'}

@render_to('liveq/_table.html')
def x_open_sessions(request):
    return {'sessions': Session.objects.filter(status__id__in=[STATUS_OPEN, STATUS_CLOSED]),
            'mode': 'open'}

@render_to('liveq/_table.html')
def x_talk_sessions(request):
    return {'sessions': Session.objects.filter(status__id=STATUS_TALK),
            'mode': 'talk'}

@render_to('liveq/_table.html')
def x_finished_sessions(request):
    return {'sessions': Session.objects.filter(status__id=STATUS_FINISHED),
            'mode':'finished'}



@render_to('liveq/tablo.html')
def open_sessions(request):
    now = datetime.now()
    sessions = Session.objects.filter(status__id__in=[STATUS_OPEN, STATUS_CLOSED])
    if not sessions.count() or not sessions[0].begins_after():
        check_statuses(now)
        sessions = Session.objects.filter(status__id__in=[STATUS_OPEN, STATUS_CLOSED])
    return {
            'sessions': sessions,
            'now': now,
            'timeout_interval': TIMEOUT_INTERVALS['open'],
            'refresh_url': reverse('x_open_sessions'),
            'mode': 'open',
            'content_title': u'Сеансы чтения, на которые можно записаться',
            }



@render_to('liveq/tablo.html')
def talk_sessions(request):
    now = datetime.now()
    return {
            'sessions': Session.objects.filter(status__id=STATUS_TALK),
            'now': now,
            'timeout_interval': TIMEOUT_INTERVALS['talk'],
            'refresh_url': reverse('x_talk_sessions'),
            'content_title': u'Идёт разговор',
            'mode': 'talk',
            }



@render_to('liveq/tablo.html')
def finished_sessions(request):
    now = datetime.now()
    return {
            'sessions': Session.objects.filter(status__id__in=[STATUS_OPEN, STATUS_CLOSED]),
            'now': now,
            'timeout_interval': TIMEOUT_INTERVALS['finished'],
            'refresh_url': reverse('x_finished_sessions'),
            'content_title': u'Завершенные сеансы чтения',
            'mode': 'finished',
            }


def context_processor(request):
    return {
            'STATUS_OPEN': STATUS_OPEN,
            'STATUS_CLOSED': STATUS_CLOSED,
            'STATUS_FINISHED': STATUS_FINISHED,
            'STATUS_TALK': STATUS_TALK,
            }


@login_required
def x_add(request):
    new_session = None
    message = bad(u"Не POST-запрос")
    html, status = '', 'errors'
    if request.method == 'POST':
        form = SessionForm(request.POST)
        if form.is_valid():
            message = ''
            ids = request.POST['reader_ids'].split(',')

            if type(ids) != list: ids = [ids]
            readers =  Reader.objects.filter(number__in=set(ids))
            # читатели с несуществующими номерами проигнорированы
            true_readers = []
            for r in readers:
                is_reading_now, csession =  r.can_read_more()
                if is_reading_now: # читатель уже где-то записан
                    message += bad(u"%s в группе %s" % (r, csession))
                    continue
                true_readers.append(r)

            if not len(readers):
                message += bad(u"Читатели с номерами %s не зарегистрированы"% ",".join(ids))

            new_session = form.save()
            for r in true_readers:
                new_session.readers.add(r)
            status = "OK"
            message += good(u"Новый сеанс чтения создан")
            html = render_to_string('liveq/_session_tag.html', 
                        RequestContext(request, {'session': new_session,}))
        else:
            message = bad(u"Ошибки в форме")
            html = form.errors.as_ul()
    
    return json_response({'message': message,
        'status': status,
        'html': html})


@login_required
def x_update_status(request):
    if request.method == 'POST':
        session = Session.objects.get(pk=request.POST['id'])
        try:
            session.status = Status.objects.get(pk=request.POST['status'])
        except DoesNotExist:
            return html_response(bad(u"Ошибка изменения статуса для %s") % session)
        session.save()
        return html_response(good(u"%s: %s")% (session, session.status))
    return html_response(bad(u"Не POST-запрос"))


@login_required
def x_update_reader(request):
    if request.method == 'POST':
        number = int(request.POST['number'])
        if not number:
            return html_response(bad(u"Номер читателя должен быть числом (не %s)" % request.POST['number']))
        reader, new = Reader.objects.get_or_create(number=number)
        old_name = '' if new else u' (прежнее имя %s)' % reader.name
        reader.name = request.POST['name']
        reader.save()
        return html_response(good(u"Новое имя: %s%s" % (reader, old_name)))
    return html_response(bad(u"Не POST-запрос"))


@login_required
def x_update_start_time(request):
    if request.method == 'POST':
        session = Session.objects.get(pk=request.POST['id'])
        session.start = datetime.strptime(request.POST['time_val'], settings.PYTHON_DATETIME_FORMAT)
        session.finish = session.start + timedelta(seconds=SESSION_LENGTH)
        session.save()
        return json_response({'message':good(u"Новое время разговора: %s -- %s" % (
            session.start.strftime(settings.TIME_FORMAT),
            session.finish.strftime(settings.TIME_FORMAT)
            )),
            'start_time':session.start.strftime(settings.TIME_FORMAT),
            'finish_time':session.finish.strftime(settings.TIME_FORMAT),
            })
    return html_response(bad(u"Не POST-запрос"))


@login_required
def x_update_finish_time(request):
    if request.method == 'POST':
        session = Session.objects.get(pk=request.POST['id'])
        session.finish = datetime.strptime(request.POST['time_val'], settings.PYTHON_DATETIME_FORMAT)
        if session.finish < session.start: 
            return html_response(bad(u"Окончание раньше начала"))
        session.save()
        return json_response({'message':good(u"Новое время разговора: %s -- %s" % (
            session.start.strftime(settings.TIME_FORMAT),
            session.finish.strftime(settings.TIME_FORMAT)
            )),
            'start_time':session.start.strftime(settings.TIME_FORMAT),
            'finish_time':session.finish.strftime(settings.TIME_FORMAT),
            })
    return html_response(bad(u"Не POST-запрос"))

@login_required
def x_add_reader(request):
    if request.method == 'POST':
        session = Session.objects.get(pk=request.POST['id'])
        reader, new = Reader.objects.get_or_create(number=request.POST['new_reader'])
        if reader in session.readers.all():
            return html_response(good(u"В этой группе уже есть такой читатель"))
        is_reading_now, csession =  reader.can_read_more()
        if is_reading_now: # читатель уже где-то записан
            return html_response(bad(u"%s в группе %s" % (reader, csession)))
        session.readers.add(reader)
        return html_response(render_to_string('liveq/_reader_tag.html', 
            RequestContext(request, {'reader':reader})))
    return html_response(bad(u"Не POST-запрос"))


@login_required
def x_delete_reader(request):
    if request.method == 'POST':
        session = Session.objects.get(pk=request.POST['id'])
        reader = Reader.objects.get(pk=request.POST['reader'])
        session.readers.remove(reader)
    return html_response(u"<span class='info'>Вы удалили читателя %s</span>" % reader)

