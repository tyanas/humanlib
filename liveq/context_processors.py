# -*- encoding: utf-8 -*-
# Copyright 2010 CHTD
# This file is part of chtd_context.

# chtd_context is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# chtd_context is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with chtd_context.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings

def css_theme(request):
    prefix = settings.MEDIA_URL+settings.THEMES_FOLDER+settings.THEME_NAME
    STATIC_URL = getattr(settings, 'STATIC_URL', '') or ''
    l = [("{0}_MEDIA_URL".format(x.upper()),
          "{0}/{1}/".format(prefix, x)) for x in ('css','img','js')]
    l.append(("STATIC_JS_MEDIA_URL", STATIC_URL + 'js/'))
    l.append(("THEME_NAME", settings.THEME_NAME))
    l.append(("THEME_TPL_TYPE", getattr(settings,'THEME_TPL_TYPE','') or ''))
    return dict(l) 

def datetime_formats(request):
    return {
            'PYTHON_DATETIME_FORMAT': getattr(settings, 
                'PYTHON_DATETIME_FORMAT', "%Y-%m-%d %H:%M:%S"), 
            'DJANGO_DATETIME_FORMAT': getattr(settings, 
                'DJANGO_DATETIME_FORMAT', None), 
            'DJANGO_DATE_FORMAT':     getattr(settings, 
                'DJANGO_DATE_FORMAT', None),     
            'TEMPLATE_DATETIME_FORMATS':     getattr(settings, 
                'TEMPLATE_DATETIME_FORMATS', {}),     
            }
