# -*- encoding: utf-8 -*- 
# Copyright 2010 CHTD
# This file is part of chtd_tags.

# chtd_tags is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# chtd_tags is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with chtd_tags.  If not, see <http://www.gnu.org/licenses/>.

from django import template
import re

register = template.Library()

@register.simple_tag
def active(request, pattern):
    """ Проверяет наличие pattern'а в request """
    if re.search(pattern, request.path):
        return 'active'
    return ''
