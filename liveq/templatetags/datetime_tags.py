# -*- encoding: utf-8 -*-
# Copyright 2010 CHTD
# This file is part of chtd_tags.

# chtd_tags is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# chtd_tags is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with chtd_tags.  If not, see <http://www.gnu.org/licenses/>.

import datetime
        
from django import template
from django.utils import dateformat 
from django.conf import settings

register = template.Library()

python_datetime_format = getattr(settings, 'PYTHON_DATETIME_FORMAT', "%Y-%m-%d %H:%M:%S")
django_datetime_format = getattr(settings, 'DJANGO_DATETIME_FORMAT', None)
django_date_format = getattr(settings, 'DJANGO_DATE_FORMAT', None)


@register.filter
def project_datetime(value, arg = django_datetime_format,
                     project_dt_format = python_datetime_format,
                     empty_string = "..."):
    """ Форматирует datetime stamp в соответствии с настройками проекта,
    формат даты и времени берется по умолчанию их settings.DJANGO_DATETIME_FORMAT """
    assert arg
    if not value:
        # undefined date/time (in edit forms) 
        return empty_string
    try:
        # если у value есть все нужные атрибуты (требуемые в arg), 
        # просто показываем дату в нужном формате 
        return dateformat.format(value, arg)
    except AttributeError:
        try:
            # если value -- это строка, то, возможно, в ней дата записана
            # в формате проекта. Пытаемся прочитать её.
            val = datetime.datetime.strptime(value, project_dt_format)
            return dateformat.format(val, arg)
        except (TypeError, ValueError):
            # если не можем прочитать value как дату-время
            return value


@register.filter
def project_date(value):
    return project_datetime(value, django_date_format)


month_names = u'января февраля марта апреля мая июня '\
              u'июля августа сентября октября ноября декабря'.split()

assert len(month_names) == 12

@register.filter
def native_date(value):
    ''' Форматирует дату в виде "12 июля 2010" правильно склоняя месяц '''
    value = value or datetime.datetime.now()
    return u'{day} {month} {year}'.format(
        day = value.day,
        month = month_names[value.month - 1],
        year = value.year)
    
