# -*- encoding: utf-8 -*-

from django.contrib import admin
from django.contrib.auth.models import User, UserManager
from django.contrib.auth.admin import UserAdmin

from liveq.models import Book, Reader, Session, Status

admin.site.register(Book)

class ReaderAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'name', 'number', 'total_sessions', 'sessions_as_str')
    ordering = ['number']

admin.site.register(Reader, ReaderAdmin)

class SessionAdmin(admin.ModelAdmin):
    list_display = ('start', 'begins_after', 'book', 'readers_as_str', 'status')
    search_fields = ['book']
    ordering = ['-start']
    list_filter = ('status', 'book', )

admin.site.register(Session,SessionAdmin)

admin.site.register(Status)
