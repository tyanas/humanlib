# -*- encoding: utf-8 -*-
import os.path
from django_assets import Bundle, register
from django.conf import settings

STATIC_URL = ''
STATIC_THEME_URL = os.path.join(STATIC_URL, settings.THEME_URL)

def add_prefix(*args,**kwargs):
    new_args = []
    prefix = kwargs.get('prefix', STATIC_URL)
    for a in args:
        new_args.append('%s%s' % (prefix, a))
    return new_args

common_css = Bundle(
        *add_prefix(
            'css/reset.css',
            'css/util.css',
            'css/hidden_onload.css',
            'css/tables.css',
            'css/icons.css',
            'css/will_pickdate.css',
            ),
        filters='cssrewrite,cssutils'
        )
theme_css = Bundle(
        *add_prefix(
            'css/layout.css',
            'css/base.css',
            'css/blocks.css',
            'css/tables.css',
            'css/dropdownmenu.css',
            'css/gradients.css',
            prefix=STATIC_THEME_URL
            ),
        filters='cssrewrite,cssutils'
        )

common_js = Bundle(
        *add_prefix(
            'js/jquery-1.6.1.js',
            'js/jquery.mousewheel.js',
            # date and time picker
            'js/will_pickdate.js',
            'js/init-tables.js',
            'js/async.js',
            ),
        filters='rjsmin'
        )

all_css = Bundle(
        common_css,
        theme_css,
        output='cache/packed.js'
        )

all_js = Bundle(
        common_js,
        output='cache/packed.js'
        )

register('css_all', all_css) 
register('js_all', all_js) 
