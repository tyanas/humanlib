# -*- coding: utf-8 -*-

from django import forms

from liveq.models import Book, Reader, Session

class SessionForm(forms.ModelForm):
    class Meta:
        model = Session
        fields = ('book',)



class ReaderForm(forms.ModelForm):
    class Meta:
        model = Reader
        fields = ('number', 'name')
        widgets = {
                'number': forms.TextInput(attrs={'size': 3}),
                'name': forms.TextInput(attrs={'size': 6}),
                }



