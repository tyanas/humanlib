from django.conf.urls.defaults import patterns, include, url
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'liveq.views.open_sessions', name='index'),
    url(r'^talk/$', 'liveq.views.talk_sessions', name='talk_sessions'),
    url(r'^finished/$', 'liveq.views.finished_sessions', name='finished_sessions'),
    url(r'^$', 'liveq.views.open_sessions', name='index'),
    url(r'^add/$', 'liveq.views.add', name='add'),
    url(r'^x/edit/sessions/$', 'liveq.views.x_edit_sessions', name='x_edit_sessions'),
    url(r'^x/open/sessions/$', 'liveq.views.x_open_sessions', name='x_open_sessions'),
    url(r'^x/talk/sessions/$', 'liveq.views.x_talk_sessions', name='x_talk_sessions'),
    url(r'^x/finished/sessions/$', 'liveq.views.x_finished_sessions', name='x_finished_sessions'),
    url(r'^x/add/$', 'liveq.views.x_add', name='x_add'),
    url(r'^x/check/statuses/$', 'liveq.views.x_check_statuses', name='x_check_statuses'),
    url(r'^x/add/reader/$', 'liveq.views.x_add_reader', name='x_add_reader'),
    url(r'^x/delete/reader/$', 'liveq.views.x_delete_reader', name='x_delete_reader'),
    url(r'^x/update/reader/$', 'liveq.views.x_update_reader', name='x_update_reader'),
    url(r'^x/update/start/time/$', 'liveq.views.x_update_start_time', name='x_update_start_time'),
    url(r'^x/update/finish/time/$', 'liveq.views.x_update_finish_time', name='x_update_finish_time'),
    url(r'^x/update/status/$', 'liveq.views.x_update_status', name='x_update_status'),

    url(r'^login/$', 'django.contrib.auth.views.login', name = 'login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name = 'logout'),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}, name = 'media'),
    url(r'^admin/', include(admin.site.urls)),
)
if settings.DEBUG is False:   #if DEBUG is True it will be served automatically
    urlpatterns += patterns('',
            url(r'^static/(?P<path>.*)$', 'django.views.static.serve', 
                {'document_root': settings.STATIC_ROOT}),
            )

