jQuery.fn.reset = function () {
      $(this).each (function() { this.reset(); });
      $(this).find('select').change();//чтобы показать все строки в таблице
}

function addMins(d, m) {
    return new Date(d.valueOf()+m*60000);
}

function _getTime() {
    return formatTime(new Date());
}

function addZero(n) {
    return n < 10 ? "0"+n : n;
}
function formatTime(currentTime){
    var hours = currentTime.getHours();
    var mins = currentTime.getMinutes();
    var secs = currentTime.getSeconds();
    
    return addZero(hours) + ":" + addZero(mins) + ":" + addZero(secs);
}

function formatDate(currentTime){
    var year = currentTime.getFullYear();
    var month = 1+currentTime.getMonth();
    var day = currentTime.getDate();

    return year + "-" + addZero(month) + "-" + addZero(day);
}

/* refresh */
var timeoutId = null;

function refresh_queue() {
    if (typeof REFRESH_URL === 'undefined') return;
    if (typeof LOADING_HTML !== 'undefined') {
        $('.indicator').html(LOADING_HTML);
    }
    $('.livequeue').load(REFRESH_URL, function(){
        add_handlers();
        $('.new_reader_form').find('.info, .errors').remove();
        $('.indicator').html('Обновлено. Следующий раз через <b class="sec">'+(TIMEOUT_INTERVAL/1000)+'</b> секунд '+_getTime());
        clockStart();
        });
}

function refreshOn() {
    if (typeof TIMEOUT_INTERVAL === 'undefined') return;
    timeoutId = setInterval(refresh_queue, TIMEOUT_INTERVAL);
    $('.indicator').html('Через  <b class="sec">'+(TIMEOUT_INTERVAL/1000)+'</b> секунд обновлю таблицу '+_getTime());
    clockStart();
}

function refreshOff() {
    if (typeof timeoutId !== 'undefined' && timeoutId != null) {
        clearInterval(timeoutId);
        $('.indicator').html('Жду, пока редактируешь '+_getTime());
    } else {
        $('.indicator').html('Обновления не было включено :( '+_getTime());
    }
    clockStop();
}


/*clock*/
var timerId; // таймер, если часы запущены
 
function clockStart() {  // запустить часы
      if (timerId) return;
       
        timerId = setInterval(update, 1000);
          update();  // (*)
}
 
function clockStop() {
      clearInterval(timerId);
        timerId = null;
}
function update() {
    var clock = $('.indicator .sec');
    clock.html(parseInt(clock.html())-1);
}
/**/
var a_reader_pattern = /^\s*\d[\d\s]*$/
var readers_pattern = /^\s*\d[\d\s,]*$/

function show_message(msg){
    msg += " "+_getTime();
    $(".request_progress").html(msg);
    $(".request_progress").show();//fadeIn().fadeOut(5000);
}

function warn(obj,msg){
    msg += " "+_getTime();
    obj.after('<span class="errors">'+msg+'</span>');
}

function info(obj,msg){
    msg += " "+_getTime();
    obj.after('<span class="info">'+msg+'</span>').slideToggle(300);
}

var last_click = null;

function post_process(obj) {
    obj
        .success(function(data){
            if (data[0] == "{") { data = $.parseJSON(data)['message']; }
            show_message('Готово!  '+data);
        })
        .error  (function(data){
            if (data[0] == "{") { data = $.parseJSON(data)['message']; }
            show_message('Ошибка :('+data);
            if (last_click) {
                last_click.parent().addClass('failed');
                last_click = null;
            }
        }); 
}


function update_time(obj){
    // Изменение времени начала или окончания сессии
    STOP_SUBMIT = false;
    var ctime = new Date(),
        btn_pair = 'finish_time',
        btn = $(obj.target); // last_click;
    jqXHR = $.post(btn.attr('url'), {
        'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(),
        'time_val': formatDate(ctime)+" "+btn.val()+":00",
        'id': btn.parents('tr').attr('session_id')
        }, function(data){
            btn.parent().addClass('success').removeClass('failed');
             
            if (data[0] == "{") { 
                data = $.parseJSON(data);
                if (btn.attr('id') == btn_pair) btn_pair = 'start_time';
                btn.parent().parent().find('[sid='+btn_pair+']').val(data[btn_pair]);
            }
        }); 
    post_process(jqXHR);
    refreshOn();
}


function update_status(event){
    // Изменение статуса
    var cell = $(event.target).parent(), 
        stat = cell.attr('status_id'),
        row = cell.parent(),
        new_stat = (stat - 1) ? STATUS_OPEN : STATUS_CLOSED;
    jqXHR = $.post('/x/update/status/', {
        'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(),
        'status': new_stat,
        'id': row.attr('session_id')
        }, function(){
            cell.addClass('success').removeClass('failed')
                .attr('status_id', new_stat)
                .find('span').hide();
            cell.find('span.status'+new_stat).show();
            if (new_stat == 2) {
                row.find('input.add_reader').hide();
            } else {
                row.find('input.add_reader').show();
            }
        }); 
    post_process(jqXHR);
}

function update_session(event){
    var t = event.target,
        node = event.target.nodeName;
    if (node =='TD') return; // just speedup
    if ($(t).hasClass('select_time')) {
        last_click = $(t);
        $(t).parent().removeClass('success').removeClass('failed');
    }
    if (node == 'INPUT') {
        var btn = $(t).filter(':visible');
        if (btn.hasClass('add_reader')) {
            clean_table();
            refreshOff();
            btn.fadeOut().parent()
                .find('[name=new_reader]').show().focus().val('');
            //$('#new_session p').click(clean_table);
        } else if (btn.hasClass('delete_reader')) {
            // Удаление читателя из Сессии
            if (confirm("Удалить читателя?")) {
                jqXHR = $.post('/x/delete/reader/', {
                    'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(),
                    'reader':btn.parent().attr('reader_id'),
                    'id': btn.parents('tr').attr('session_id')
                    }, function(data){
                        btn.parent().after(data).remove();
                    }); 
                post_process(jqXHR);
            }
        } else if (btn.hasClass('select_time')) {
            refreshOff();
        }
    } else if (node == 'SPAN') {
        // клик удаляет информационные блоки
        if($(t).hasClass('info') || $(t).hasClass('errors')) {
            $(t).remove();
        }
    }
}

var STOP_SUBMIT = false;

function submit_form(event){
    if (STOP_SUBMIT) return false;
    event.preventDefault();
    var f = $(this),
        sbm = f.find('[type=submit]'),
        num = f.find('[name=number]'),
        rdr = f.find('[name=name]'),
        inp = f.find('[name=new_reader]:visible');
    if (inp.length && !inp.val().length) {
        clean_table();
        inp = [];
    }
    if (!inp.length && !num.val().length) {
        warn(sbm, "Номер читателя?");
        return
    }
    if (inp.length) {
        // Добавление нового читателя
        if (inp.val().match(a_reader_pattern) == null) {
            warn(inp, "Введите номер читателя");
            return
        }
        jqXHR = $.post('/x/add/reader/', {
            'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(),
            'new_reader': inp.val().trim(),
            'id': inp.parents('tr').attr('session_id')
            }, function(data){
                inp.fadeOut().siblings('.add_reader').fadeIn().before(data);
            });
        refreshOn();
    } else if (num.val() || rdr.val()){
        // Обновление имени читателя
        if (num.val().match(a_reader_pattern) == null) {
            warn(sbm, "Номер читателя должен быть числом");
            return
        }
        if (rdr.val().length < 2) {
            warn(sbm, "Введите имя читателя");
            return
        }
        jqXHR = $.post('/x/update/reader/', {
            'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(),
            'name': rdr.val().trim(),
            'number': num.val()
            }, function(data){
                f.reset();
                f.find('.errors').remove();
            });
        refreshOn();
    } 
    post_process(jqXHR);
}

function clean_table() {
    $('[name=new_reader]:visible').each(function(){
        $(this).val('').hide();
        $(this).siblings('.add_reader').fadeIn();
        // можно забить если таблица будет обновляться по timeout
        $(this).parent().find('.errors, .info').remove();
    });
}

function check_statuses(){
    var jqXHR = $.ajax('/x/check/statuses/');
    post_process(jqXHR);
    // увидеть сообщение и освежить страницу
    refresh_queue();
}

WILL_PICKER_OPTIONS = {
            format: 'H:i',
            inputOutputFormat: 'H:i',
            onClose: update_time,
            useFadeInOut: false, // don't destroy
            showHeader:false,
            militaryTime: true,//ampm??
            positionOffset: { x: -50, y: -30 },
            timePickerOnly: true
        };

function add_handlers(){
    var $queue = $('.livequeue');
    $queue.click(update_session);
    $queue.find('[type=checkbox]').change(update_status);
    //$queue.find('.select_time').keyup(update_time);
    $queue.find('.select_time').focus(function(){STOP_SUBMIT = true;});
    $queue.find('.select_time').blur(update_time); // .will_pickdate(WILL_PICKER_OPTIONS);
    zebra_lines($queue);
}

$(document).ready(function(){
    refreshOn();
    $('#new_session').submit(submit_form);
    $('#new_session select, .new_reader_form input').focus(refreshOff).blur(refreshOn);
    $('#new_session [name=book]').change(function(){
        $('.livequeue table tr').show();
        if ($(this).val()) {
            $('.livequeue table tr').not('[book_id='+$(this).val()+']').hide();
        }
    });
    $('#check_statuses').click(check_statuses);
    //setTimeout(check_statuses, 120000);// раз в две минуты 
    add_handlers();
});
