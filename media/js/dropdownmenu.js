/*
 * http://css-tricks.com/simple-jquery-dropdowns/
 */
$(function(){

    $("ul.dropdown li").hover(function(){
    
        $(this).addClass("hover");
        $('ul:first',this).css('visibility', 'visible');
    
    }, function(){
    
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
    
    });
    
    //$("ul.dropdown a.todo").attr("title", "В разработке");

});
