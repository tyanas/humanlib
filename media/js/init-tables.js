
function zebra_lines(table){
    $('tbody tr:visible th[rowspan]',table).addClass('force_th_bg');
    $('input,select',table);
    $('tbody tr:visible',table)
        .focusin(function(){
        $(this).addClass('highlight_on_focus');})
        .focusout(function(){
        $(this).removeClass('highlight_on_focus');})
        .hover(
            function () {$(this).toggleClass('highlight');},
            function () {$(this).toggleClass('highlight');})
        .filter(':odd') // add color to even lines only
        .removeClass('odd').removeClass('even')
        .filter(':odd').addClass('odd').end()
        .filter(':even').addClass('even');
}

$(document).ready(function() {
    /* Порядок инициализации важен. Например, просмотр истории значений 
     * делает колонки шире, 
     * поэтому фиксировать шапку нужно после него:
     *      перед показом фиксированной шапки запоминается 
     *      ширина колонок таблицы
     **/
    
    // 
    zebra_lines($('#content .zebra')); 

   
}); 
