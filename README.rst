Живая очередь для `Живой библиотеки`_.

`Demo`_

Quick queue.
This service was written in 2011 for a `Human library`_ event in
`Nizhniy Novgorod`_. Also it was used in `2012`_ and `2013`_.

Use ``admin`` / ``admin`` to `log in`_ as a manager. And you'll be able to start / stop
reading sessions, register readers.

.. _Human library: http://humanlibrary.org/
.. _Demo: http://nn.hl.tyanas.ru/
.. _log in: http://nn.hl.tyanas.ru/login/
.. _Nizhniy Novgorod: https://vk.com/event31950939
.. _2012: https://vk.com/humanlibrary_nn
.. _2013: https://vk.com/event49283395
.. _Живой библиотеки: http://tinyurl.com/blwf3om
