# placed in a separate file to prevert cyclic import

def check_output_uniqueness(*groups_list):
    ''' Check output filenames for uniqueness.
    Sample usage: place it at the end of settings.py
    from compress.utils import check_output_uniqueness
    check_output_uniqueness(COMPRESS_JS, COMPRESS_CSS) '''
    output_filenames = set()
    for groups in groups_list:
        for name, data in groups.iteritems():
            if data['output_filename'] in output_filenames:
                raise Exception(
                    'Compress error: Duplicate output filename %s (group %s)' % (
                        data['output_filename'], name))
            output_filenames.add(data['output_filename'])
