# -*- encoding: utf-8 -*-

import os.path
import conf
from shortcuts import compress_css_dict, compress_js_dict

DEBUG = conf.DEBUG
TEMPLATE_DEBUG = DEBUG

ROOT = conf.ROOT

ADMINS = conf.ADMINS
MANAGERS = ADMINS

LOGIN_URL='/login/'
LOGIN_REDIRECT_URL='/'

SERIALIZATION_MODULES = {'csv' : 'django_snippets.csv_serializer'}

DATABASES = conf.DATABASES

TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru-RU'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
MEDIA_URL = '/media/'
MEDIA_ROOT = '{0}{1}'.format(ROOT,MEDIA_URL)
ADMIN_MEDIA_PREFIX = '/media/admin/'

PYTHON_DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
TIME_FORMAT = '%H:%M'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/static/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'od)f8v0)dt-%1+x(h!&7s1zv9)m=j@e6q55ar9xe(%i32-vk^#'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'liveq.context_processors.css_theme',
    'liveq.views.context_processor',
    )
# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    os.path.join(ROOT, 'templates'),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django_extensions',
    #'django_assets',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    #'chtd_context',
    #'chtd_tags',
    'compress',
    #'south',
    'liveq',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# skin settings
THEMES_FOLDER = 'themes/'
THEME_NAME = conf.THEME_NAME
THEME_URL = THEMES_FOLDER + THEME_NAME + '/'

# compress
COMPRESS_CSS = {
    'common_css':       compress_css_dict('common',(
        'reset',
        'util',
        'hidden_onload',
        'tables',
        'icons',
        'will_pickdate',
        )),
    'base_css':         compress_css_dict('base',(
        'layout',
        'base',
        'blocks',
        'tables',
        'dropdownmenu',
        #'gradients',
        ),THEME_URL),
    }
 
COMPRESS_JS = {
    'base_js': compress_js_dict('base',(
        'jquery-1.6.1',
        'jquery.mousewheel',
        # date and time picker
        'will_pickdate',
        'init-tables',
        'async',
        )),
    }
