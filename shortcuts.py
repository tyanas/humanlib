import os

def compress_css_dict(name, filenames=(), prefix='', media='all'):
    ''' shortcut for COMPRESS_CSS group descriptions '''
    if not filenames: filenames = (name,)
    return compress_common_dict(['css/%s.css' % x for x in filenames],
            'css/%s_compressed.?.css' % name, prefix, media)

def compress_js_dict(name, filenames=(), prefix='', media='all'):
    ''' shortcut for COMPRESS_JS group descriptions '''
    if not filenames: filenames = (name,)
    return compress_common_dict(['js/%s.js' % x for x in filenames],
            'js/%s_compressed.?.js' % name, prefix, media)

def compress_common_dict(source_filenames, output_filename, 
        prefix='', media='all'):
    ''' shortcut for COMPRESS_{J,CS}S group descriptions '''

    return {'source_filenames':[os.path.join(prefix, x) 
        for x in source_filenames],
            'output_filename': os.path.join(prefix, output_filename),
            'extra_context': {'media': media,}
            } 
